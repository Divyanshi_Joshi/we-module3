import pygame
from pygame.locals import *
import random
from strategies import Strategies
from ui import UI

class Gameplay:
    def __init__(self, name):
        self.player_name = name
        self.computer_name = "Computer"
        self.round_number = 1
        self.player_points = 0
        self.computer_points = 0
        self.player_hand = [f"{i}_of_hearts" for i in ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king', 'ace']]
        self.computer_hand = [f"{i}_of_clubs" for i in ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king', 'ace']]
        self.diamond_cards = [f"{i}_of_diamonds" for i in ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king', 'ace']]
        self.auction_card = random.choice(self.diamond_cards)
        self.strategies = Strategies()
        self.points = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'jack': 10, 'queen': 10, 'king': 10, 'ace': 10}
        self.ui = UI()

    def run_game(self):
        pygame.init()
        screen_width = 1200
        screen_height = 600
        screen = pygame.display.set_mode((screen_width, screen_height))

        running = True
        while running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    running = False

                if event.type == MOUSEBUTTONDOWN:
                    player_bid = self.ui.handle_mouse_click(pygame.mouse.get_pos(), self.player_hand)
                    if player_bid is not None:
                        computer_bid = self.strategies.comp_strategy(self.computer_hand, self.round_number, self.auction_card)
                        self.compare_bids(player_bid, computer_bid)

                        self.player_hand.remove(player_bid)
                        self.computer_hand.remove(computer_bid)

                        self.round_number += 1
                        if self.round_number <= 13:
                            self.auction_card = random.choice(self.diamond_cards)

            self.ui.update_screen(screen, self.player_name, self.player_points, self.player_hand, self.computer_name, self.computer_points, self.computer_hand, self.round_number, self.auction_card)

        pygame.quit()

    def compare_bids(self, player_bid, computer_bid):
        player_card_rank = player_bid.split('_')[0]
        computer_card_rank = computer_bid.split('_')[0]

        if self.points[player_card_rank] > self.points[computer_card_rank]:
            self.player_points += self.points[self.auction_card.split('_')[0]]  # Add the points from the diamond card
        elif self.points[computer_card_rank] > self.points[player_card_rank]:
            self.computer_points += self.points[self.auction_card.split('_')[0]]  # Add the points from the diamond card
        else:  # If there's a tie
            self.player_points += self.points[self.auction_card.split('_')[0]] // 2  # Divide the points from the diamond card equally
            self.computer_points += self.points[self.auction_card.split('_')[0]] // 2  # Divide the points from the diamond card equally

