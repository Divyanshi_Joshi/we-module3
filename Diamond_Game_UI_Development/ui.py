import pygame
from pygame.locals import *
import random

class UI:
    def __init__(self):
        self.points = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'jack': 10, 'queen': 10, 'king': 10, 'ace': 10}
        self.cards = {f"{i}_of_{j}": pygame.transform.scale(pygame.image.load(f"cards/{i}_of_{j}.png"), (72, 96)) for i in ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king', 'ace'] for j in ['hearts', 'clubs', 'diamonds']}

    def update_screen(self, screen, player_name, player_points, player_hand, computer_name, computer_points, computer_hand, round_number, auction_card):
        # Fill the screen with a color
        screen.fill((255, 255, 255))

        # Display player information
        font = pygame.font.Font(None, 36)
        text = font.render(f"Name: {player_name} Points: {player_points}", True, (0, 0, 0))
        screen.blit(text, (20, 20))

        # Display player's hand
        for i, card in enumerate(player_hand):
            screen.blit(self.cards[card], (20 + i * 80, 60))  # Adjusted the spacing between cards

        # Display computer information
        text = font.render(f"Name: {computer_name} Points: {computer_points}", True, (0, 0, 0))
        screen_width = 1200
        screen_height = 600
        screen.blit(text, (20, screen_height - 130))  # Display at the bottom of the screen

        # Display computer's hand
        for i, card in enumerate(computer_hand):
            screen.blit(self.cards[card], (20 + i * 80, screen_height - 100))  # Display at the bottom of the screen

        # Display Diamond Card Auction Section
        text = font.render(f"Round: {round_number}", True, (0, 0, 0))
        screen.blit(text, (screen_width // 2, screen_height // 2 - 20))  # Display in the middle of the screen
        if round_number <= 13:  # If there are still rounds left
            screen.blit(self.cards[auction_card], (screen_width // 2, screen_height // 2 + 20))  # Display in the middle of the screen
        else:  # If all rounds have been played
            if player_points > computer_points:
                winner = player_name
                text = font.render(f"The game has ended. {winner} wins!", True, (0, 0, 0))
            elif computer_points > player_points:
                winner = computer_name
                text = font.render(f"The game has ended. {winner} wins!", True, (0, 0, 0))
            else:
                winner = "It's a tie!"
                text = font.render(f"The game has ended. {winner}", True, (0, 0, 0))
            screen.blit(text, (screen_width // 2, screen_height // 2 + 20))  # Display in the middle of the screen

        # Update the display
        pygame.display.update()

    def handle_mouse_click(self, mouse_pos, player_hand):
        # Check if a card from the player's hand was clicked
        for i, card in enumerate(player_hand):
            if pygame.Rect(20 + i * 80, 60, 72, 96).collidepoint(mouse_pos):
                return card
        # Return None if the click doesn't correspond to any card
        return None
